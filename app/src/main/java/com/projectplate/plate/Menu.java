package com.projectplate.plate;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONObject;

import java.util.List;


/**
 * Created by andre on 6/18/2016.
 *//**/
public class Menu extends AppCompatActivity{

    public boolean changeNext = false;
    Button next;
    private GoogleApiClient mGoogleApiClient;
    private PlacesAutoCompleteAdapter mPlacesAdapter;
    String place;
    AutoCompleteTextView enterloc;
    DrawerLayout drawerLayout;
    Typeface caviardreams_regular;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Places.GEO_DATA_API)
                .build();
        mGoogleApiClient.connect();
        mPlacesAdapter = new PlacesAutoCompleteAdapter(this, android.R.layout.simple_dropdown_item_1line, mGoogleApiClient,
                new LatLngBounds(new LatLng(24.607069, -68.598633), new LatLng(48.516604, -125.101318)), null);

        setContentView(R.layout.menu);
        caviardreams_regular = Typeface.createFromAsset(getAssets(), "fonts/caviardreams.ttf");
        Typeface caviardreams_bold = Typeface.createFromAsset(getAssets(), "fonts/caviardreams_bold.ttf");
        Typeface caviardreams_italic = Typeface.createFromAsset(getAssets(), "fonts/caviardreams_italic.ttf");

        if (ContextCompat.checkSelfPermission(Menu.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(Menu.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Menu.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    2);
        }

        //Setup drawer layout/toolbar
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LayoutInflater inflater = LayoutInflater.from(this);
        View v = inflater.inflate(R.layout.titleview, null);
        getSupportActionBar().setCustomView(v, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
        ));
        TextView title = (TextView)v.findViewById(R.id.title);
        title.setText("Menu");
        title.setTypeface(caviardreams_regular);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_opened, R.string.drawer_closed);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        //Setup ListView
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
                drawer.closeDrawer(GravityCompat.START);
                if(id==R.id.nav_contact){
                    startActivity(new Intent(Menu.this, ContactUs.class));
                    return true;
                }
                else if(id==R.id.nav_recent){
                    startActivity(new Intent(Menu.this, Recents.class));
                    return true;
                }
                else if(id==R.id.nav_favorites){
                    startActivity(new Intent(Menu.this, Favorites.class));
                    return true;
                }
                else if(id==R.id.nav_share){
                    Snackbar sb = Snackbar.make(drawer, "Functionality Coming Soon!",Snackbar.LENGTH_SHORT);
                    sb.show();
                    TextView message = (TextView) sb.getView().findViewById(android.support.design.R.id.snackbar_text);
                    TextView action = (TextView)sb.getView().findViewById(android.support.design.R.id.snackbar_action);
                    action.setTypeface(caviardreams_regular);
                    message.setTypeface(caviardreams_regular);
                }

                return true;
            }
        });

        //Setup Buttons & Intents
        TextView findloc = (TextView) findViewById(R.id.findlocation);
        findloc.setTypeface(caviardreams_regular);
        next = (Button) findViewById(R.id.next1);
        next.setTypeface(caviardreams_bold);
        next.setVisibility(View.GONE);
        final Intent intent = new Intent(Menu.this, Map.class);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("location", false);
                intent.putExtra("place", place);
                startActivity(intent);
            }
        });

        //Setup AutoComplete
        enterloc = (AutoCompleteTextView) findViewById(R.id.Zip);
        enterloc.setCursorVisible(false);
        enterloc.setTypeface(caviardreams_regular);
        enterloc.setOnItemClickListener(mAutocompleteClickListener);
        enterloc.setAdapter(mPlacesAdapter);
        enterloc.clearFocus();
        enterloc.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                // you can call or do what you want with your EditText here
                if(enterloc.getText().length()>0){
                    changeNext = true;
                    place = enterloc.getText().toString();
                    float scale = getResources().getDisplayMetrics().density;
                    //ViewGroup.LayoutParams params = enterloc.getLayoutParams();
                    RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)enterloc.getLayoutParams();
                    params.width= (int)(240*scale+0.5f);
                    params.rightMargin = 30;
                    enterloc.setLayoutParams(params);

                    next.setVisibility(View.VISIBLE);
                }
                else{
                    changeNext = false;
                    float scale = getResources().getDisplayMetrics().density;

                    RelativeLayout.LayoutParams params =(RelativeLayout.LayoutParams) enterloc.getLayoutParams();
                    params.rightMargin = 0;
                    params.width= (int)(300*scale+0.5f);
                    enterloc.setLayoutParams(params);
                    next.setVisibility(View.GONE);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(enterloc.hasFocus()){
                    enterloc.setCursorVisible(true);
                }
                else{
                    enterloc.setCursorVisible(false);
                }
            }
        });
        enterloc.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE||actionId==EditorInfo.IME_ACTION_PREVIOUS){
                    enterloc.clearFocus();
                    enterloc.setCursorVisible(false);
                }
                return false;
            }
        });
        enterloc.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_BACK){
                    enterloc.clearFocus();
                    enterloc.setCursorVisible(false);
                }
                return false;
            }
        });

        //Bring Icons to Front
        ImageView imageView2 = (ImageView)findViewById(R.id.icon_search);
        imageView2.bringToFront();
        imageView2.invalidate();
        ImageView imageView = (ImageView)findViewById(R.id.icon_location);
        imageView.bringToFront();
        imageView.invalidate();
    }

    public void useMyLocation(View view){
        Intent intent = new Intent(Menu.this, Map.class);
        if (ContextCompat.checkSelfPermission(Menu.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Menu.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }
        else{
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Menu.this);
                builder.setTitle("Location Not Enabled");  // GPS not found
                builder.setMessage("Location needs to be enabled. Please enable location, close the app, and wait a minute. Would you like to do that now?"); // Want to enable?
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        finish();
                    }
                });
                builder.setNegativeButton("No", null);
                builder.create().show();
            }else{
                intent.putExtra("location", true);
                startActivity(intent);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle("Location Not Enabled");  // GPS not found
                        builder.setMessage("Location needs to be enabled. Please enable location, close the app, and wait a minute. Would you like to do that now?"); // Want to enable?
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                            }
                        });
                        builder.setNegativeButton("No", null);
                        builder.create().show();
                    }else {
                        Intent intent = new Intent(Menu.this, Map.class);
                        intent.putExtra("location", true);

                        startActivity(intent);
                    }
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){

        }
        return super.onOptionsItemSelected(item);
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlacesAutoCompleteAdapter.PlaceAutocomplete item = mPlacesAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            enterloc.setSelection(0);
            enterloc.clearFocus();
        }
    };

}
