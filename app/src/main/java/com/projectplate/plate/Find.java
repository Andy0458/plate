package com.projectplate.plate;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.mediation.customevent.CustomEvent;
import com.google.android.gms.maps.model.LatLng;
import com.nhaarman.supertooltips.ToolTip;
import com.nhaarman.supertooltips.ToolTipRelativeLayout;
import com.nhaarman.supertooltips.ToolTipView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class Find extends AppCompatActivity {

    boolean favorites_pressed;
    static double currentLat, currentLong, rad;
    ImageButton favorites;
    Typeface caviardreams, caviardreams_bold;
    private static String token;
    public static ArrayList<String> ids;
    public static Restaurant restaurant = null;
    private static int random;
    ViewPager viewPager;
    static ImageView place_photo;
    private static ArrayList<Bitmap> photos;
    private static ArrayList<Review> reviews;
    TabLayout tabLayout;
    static ToolTipRelativeLayout refresh_toolTipRelativeLayout, tab_toolTipRelativeLayout;
    static boolean isFirstRun;
    SharedPreferences wmbPreference;
    public void changeTabsFont() {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(caviardreams, Typeface.NORMAL);
                    ((TextView) tabViewChild).setAllCaps(false);
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ids = new ArrayList<>();
        Intent pastIntent = getIntent();
        currentLat = pastIntent.getDoubleExtra("latitude", 0);
        currentLong = pastIntent.getDoubleExtra("longitude", 0);
        rad = pastIntent.getDoubleExtra("radius", 5*1609.344);
        System.out.println(currentLat+", "+currentLong+": "+rad);

        setContentView(R.layout.loading);
        TextView text = (TextView)findViewById(R.id.loading);
        caviardreams = Typeface.createFromAsset(getAssets(), "fonts/caviardreams.ttf");
        text.setTypeface(caviardreams);
        CustomJSONObject.initialize();


        try {
            getToken();
        }catch (Exception e){
            e.printStackTrace();
        }



    }

    public static LatLng getLocation(){
        return new LatLng(currentLat, currentLong);
    }

    public static void setRestaurant(Restaurant r){
        restaurant = r;
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public static void setToken(String s) {
        token = s;
    }

    public void getToken(){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {

                    HttpURLConnection connection = (HttpURLConnection)((new URL("https://api.yelp.com/oauth2/token?grant_type=client_credentials&" +
                            "client_id=FFa6gUP5He_x60qEU3clfQ&" +
                            "client_secret=1yFRHk2qWjGgCcO3T8r2a6XzPNtkIMpf333WZ5htGFGslV6UBX9OM943LzS4THpE")).openConnection());
                    connection.setDoOutput(true);
                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestProperty("Accept", "application/json");
                    connection.setRequestMethod("POST");
                    connection.connect();

                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(),"UTF-8"));

                    String line = null;
                    StringBuilder sb = new StringBuilder();

                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }

                    br.close();
                    JSONObject result = new JSONObject(sb.toString());
                    Find.setToken(result.getString("access_token"));
                    System.out.println(result.getString("access_token"));
                    connection.disconnect();
                }
                catch (Exception e){
                    e.printStackTrace();
                    return null;
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void v){
                if(ids.size()>0){
                    getInfo();
                }else {
                    getRestaurants();
                }
            }
        };
        task.execute();
    }

    public void getRestaurants(){
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    OkHttpClient client = new OkHttpClient();

                    Request request = new Request.Builder()
                            .url("https://api.yelp.com/v3/businesses/search?"+
                                    "term=food"+
                                    "&categories="+getIntent().getStringExtra("categories")+
                                    "&latitude="+currentLat+
                                    "&longitude="+currentLong+
                                    "&radius="+(int)rad+
                                    "&limit=50")
                            .get()
                            .addHeader("authorization", "Bearer "+token)
                            .addHeader("cache-control", "no-cache")
                            .addHeader("postman-token", "fb3bb96a-78d9-df74-ffc0-1bad45a5e36c")
                            .build();
                    String response = client.newCall(request).execute().body().string();
                    System.out.println(response);
                    JSONObject result = new JSONObject(response);
                    for(int i = 0;i<result.getJSONArray("businesses").length();i++){
                        addId(result.getJSONArray("businesses").getJSONObject(i).getString("id"));
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }
                return null;
            }
            @Override
            protected void onPostExecute(Void v){
                if(ids.size()==0){
                    RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.loading_layout);
                    Snackbar sb = Snackbar.make(relativeLayout, "Sorry, Yelp messed up.",Snackbar.LENGTH_INDEFINITE).setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getToken();
                        }
                    });
                    sb.show();
                    TextView message = (TextView) sb.getView().findViewById(android.support.design.R.id.snackbar_text);
                    TextView action = (TextView) sb.getView().findViewById(android.support.design.R.id.snackbar_action);
                    message.setTypeface(caviardreams);
                    action.setTypeface(caviardreams);
                }else {
                    getInfo();
                }
            }
        };
        task.execute();
    }

    public void getInfo(){
        random = (int) (Math.random() * ids.size());
        AsyncTask<String, Void, Void> task = new AsyncTask<String, Void, Void>() {
            @Override
            protected Void doInBackground(String... params) {
                try {
                    String id = params[0];

                    OkHttpClient client = new OkHttpClient();

                    Request request = new Request.Builder()
                            .url("https://api.yelp.com/v3/businesses/"+id)
                            .get()
                            .addHeader("authorization", "Bearer "+token)
                            .addHeader("cache-control", "no-cache")
                            .addHeader("postman-token", "fb3bb96a-78d9-df74-ffc0-1bad45a5e36c")
                            .build();

                    String result = client.newCall(request).execute().body().string();

                    JSONObject response = new JSONObject(result);
                    System.out.println("Result: "+response);
                    try{
                        Calendar calendar = Calendar.getInstance();
                        int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
                        String hours = "Sorry, Not Open";
                        String hour_start="", min_start="", hour_end="", min_end="";
                        boolean start_pm=false, end_pm=false;
                        try{
                            JSONArray temp = response.getJSONArray("hours");
                        }catch (Exception e){
                            Intent intent = getIntent();
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                            finish();
                        }
                        for(int i = 0; i < response.getJSONArray("hours").getJSONObject(0).getJSONArray("open").length();i++){
                            if(Integer.parseInt(response.getJSONArray("hours").getJSONObject(0).getJSONArray("open").getJSONObject(i).getString("day"))==day){
                                //starting time
                                int time = Integer.parseInt(response.getJSONArray("hours").getJSONObject(0).getJSONArray("open").getJSONObject(i).getString("start"));
                                hour_start = time/100+"";
                                if(Integer.parseInt(hour_start)>=12){
                                    start_pm=true;
                                    hour_start = Integer.parseInt(hour_start)%12+"";
                                }
                                min_start = time%(Integer.parseInt(hour_start)*100)+"";
                                if(Integer.parseInt(min_start)<10){
                                    min_start = "0"+min_start;
                                }

                                //ending time
                                time = Integer.parseInt(response.getJSONArray("hours").getJSONObject(0).getJSONArray("open").getJSONObject(i).getString("end"));
                                hour_end = time/100+"";
                                min_end = time%(Integer.parseInt(hour_end)*100)+"";
                                if(Integer.parseInt(min_end)<10){
                                    min_end = "0"+min_end;
                                }
                                if(Integer.parseInt(hour_end)>=12){
                                    end_pm=true;
                                    hour_end = Integer.parseInt(hour_end)%12+"";
                                }
                            }
                        }
                        if(!hour_start.equals("")) {
                            hours = hour_start + ":" + min_start;
                            if (start_pm) {
                                hours += " pm";
                            } else {
                                hours += " am";
                            }
                            hours += " - " + hour_end + ":" + min_end;
                            if (end_pm) {
                                hours += " pm";
                            } else {
                                hours += " am";
                            }
                        }
                        LatLng latLng = new LatLng(response.getJSONObject("coordinates").getDouble("latitude"), response.getJSONObject("coordinates").getDouble("longitude"));
                        String name = response.getString("name");
                        String location = response.getJSONObject("location").getString("address1");
                        String address;
                        if(location.equals("")){
                            address = response.getJSONObject("location").getString("city");
                        }else {
                            address = response.getJSONObject("location").getString("address1") + ", " +
                                    response.getJSONObject("location").getString("city");
                        }
                        String phone = response.getString("phone");
                        ArrayList<String> photos = new ArrayList<>();
                        for(int i = 0; i < response.getJSONArray("photos").length(); i++){
                            photos.add(response.getJSONArray("photos").getString(i));
                        }
                        String site = response.getString("url");
                        double rating = response.getDouble("rating");
                        int numReviews = response.getInt("review_count");
                        Find.setRestaurant(new Restaurant(name, address, phone, site, hours, photos, rating,numReviews, latLng));

                        for(int i = 0; i < CustomJSONObject.favorites.getJSONArray("favorites").length(); i++){
                            if(CustomJSONObject.favorites.getJSONArray("favorites").getJSONObject(i).get("site").equals(site)){
                                Find.getRestaurant().setFavorite(true);
                            }
                        }
                    }catch (JSONException e){
                        e.printStackTrace();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void v){
                getReviews(random);
            }
        };
        if(ids.size()==0){
            RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.loading_layout);
            Snackbar sb = Snackbar.make(relativeLayout, "Sorry, Yelp messed up.",Snackbar.LENGTH_INDEFINITE).setAction("Try Again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getToken();
                }
            });
            sb.show();
            TextView message = (TextView) sb.getView().findViewById(android.support.design.R.id.snackbar_text);
            TextView action = (TextView)sb.getView().findViewById(android.support.design.R.id.snackbar_action);
            action.setTypeface(caviardreams);
            message.setTypeface(caviardreams);
        }else {
            task.execute(ids.get(random));
        }
    }

    public void getReviews(final int random){
        reviews = new ArrayList<>();
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                String response = "No response here :(";
                try {
                    OkHttpClient client = new OkHttpClient();

                    Request request = new Request.Builder()
                            .url("https://api.yelp.com/v3/businesses/"+ids.get(random)+"/reviews")
                            .get()
                            .addHeader("authorization", "Bearer "+token)
                            .addHeader("cache-control", "no-cache")
                            .addHeader("postman-token", "fb3bb96a-78d9-df74-ffc0-1bad45a5e36c")
                            .build();
                    response = client.newCall(request).execute().body().string();
                    System.out.println("Reviews: "+response);
                    JSONObject result = new JSONObject(response);
                    for(int i = 0;i<result.getInt("total");i++){
                        String name = result.getJSONArray("reviews").getJSONObject(i).getJSONObject("user").getString("name");
                        String review = result.getJSONArray("reviews").getJSONObject(i).getString("text");
                        String photo = result.getJSONArray("reviews").getJSONObject(i).getJSONObject("user").getString("image_url");
                        String link = result.getJSONArray("reviews").getJSONObject(i).getString("url");
                        int rating = result.getJSONArray("reviews").getJSONObject(i).getInt("rating");

                        reviews.add(new Review(name, review, photo, link, rating));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                return null;
            }
            @Override
            protected void onPostExecute(Void v){
                if(reviews.size()==0) {
                    RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.loading_layout);
                    Snackbar sb = Snackbar.make(relativeLayout, "Sorry, Yelp messed up.",Snackbar.LENGTH_INDEFINITE).setAction("Try Again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            getToken();
                        }
                    });
                    sb.show();
                    TextView message = (TextView) sb.getView().findViewById(android.support.design.R.id.snackbar_text);
                    TextView action = (TextView)sb.getView().findViewById(android.support.design.R.id.snackbar_action);
                    action.setTypeface(caviardreams);
                    message.setTypeface(caviardreams);
                }else {
                    updateUI();
                    CustomJSONObject.addRecent(restaurant);
                    CustomJSONObject.updateFiles();
                    CustomJSONObject.printRecents();
                    CustomJSONObject.printFavorites();
                }
            }
        };
        task.execute();
    }

    public void updateUI(){

        setContentView(R.layout.find_main);
//        favorites_pressed = false;
//        favorites = (ImageButton) findViewById(R.id.favoritesbutton);
//        favorites.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (favorites_pressed) {
//                    favorites.setBackgroundResource(R.drawable.favorites_static);
//                } else {
//                    favorites.setBackgroundResource(R.drawable.favorites_pressed);
//                }
//                favorites_pressed = !favorites_pressed;
//            }
//        });

        photos = new ArrayList<>();
        place_photo = (ImageView)findViewById(R.id.place_photo);
        String[] strings = new String[3];
        try{
            restaurant.getPhotoURL().toArray(strings);
        }catch (Exception e){

        }
        new DownloadImageTask().execute(strings);

        //Setup drawer layout/toolbar
        final DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        View v = inflater.inflate(R.layout.titleview, null);
        getSupportActionBar().setCustomView(v, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
        ));
        TextView title = (TextView)v.findViewById(R.id.title);
        title.setText("Find");
        title.setTypeface(caviardreams);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_opened, R.string.drawer_closed);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        //Setup ListView
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
                drawer.closeDrawer(GravityCompat.START);
                if(id==R.id.nav_contact){
                    startActivity(new Intent(Find.this, ContactUs.class));
                    return true;
                }
                else if(id==R.id.nav_recent){
                    startActivity(new Intent(Find.this, Recents.class));
                    return true;
                }
                else if(id==R.id.nav_favorites){
                    startActivity(new Intent(Find.this, Favorites.class));
                }
                else if(id==R.id.nav_share){
                    Snackbar sb = Snackbar.make(drawer, "Functionality Coming Soon!",Snackbar.LENGTH_SHORT);
                    sb.show();
                    TextView message = (TextView) sb.getView().findViewById(android.support.design.R.id.snackbar_text);
                    TextView action = (TextView)sb.getView().findViewById(android.support.design.R.id.snackbar_action);
                    action.setTypeface(caviardreams);
                    message.setTypeface(caviardreams);
                }

                return true;
            }
        });

        //Setup Tabs
        tabLayout = (TabLayout)findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        CustomPagerAdapter adapter = new CustomPagerAdapter(getSupportFragmentManager(), Find.this);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        changeTabsFont();
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                viewPager.setCurrentItem(position, false);
                if(photos.size()-1>=position) {
                    setImage(photos.get(position));
                }
                if(isFirstRun) {
                    tab_toolTipRelativeLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if(photos.size()>0) {
            place_photo.setImageBitmap(photos.get(0));
        }

        wmbPreference = PreferenceManager.getDefaultSharedPreferences(this);
        isFirstRun = wmbPreference.getBoolean("FIRSTRUN", true);
        if (isFirstRun)
        {
            View text = getLayoutInflater().inflate(R.layout.tooltip_refresh, (ViewGroup)findViewById(R.id.relativeView), false);
            TextView textView = (TextView)text.findViewById(R.id.tooltip_refresh_text);
            textView.setTypeface(caviardreams);
            refresh_toolTipRelativeLayout = (ToolTipRelativeLayout)findViewById(R.id.tooltip_refresh);
            ToolTip toolTip = new ToolTip()
                    .withShadow()
                    .withColor(Color.WHITE)
                    .withContentView(text)
                    .withAnimationType(ToolTip.AnimationType.FROM_TOP);
            ToolTipView toolTipView = refresh_toolTipRelativeLayout.showToolTipForView(toolTip, findViewById(R.id.tooltip_view));
            toolTipView.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
                @Override
                public void onToolTipViewClicked(ToolTipView toolTipView) {
                    toolTipView.remove();
                }
            });

            text = getLayoutInflater().inflate(R.layout.tooltip_tab, (ViewGroup)findViewById(R.id.relativeView), false);
            textView = (TextView)text.findViewById(R.id.tooltip_text);
            textView.setTypeface(caviardreams);
            tab_toolTipRelativeLayout = (ToolTipRelativeLayout)findViewById(R.id.tooltip_tab);
            ToolTip tab_toolTip = new ToolTip()
                    .withShadow()
                    .withColor(Color.WHITE)
                    .withContentView(text)
                    .withAnimationType(ToolTip.AnimationType.FROM_TOP);
            ToolTipView toolTipView2 = refresh_toolTipRelativeLayout.showToolTipForView(tab_toolTip, findViewById(R.id.tabs));
            toolTipView2.setOnToolTipViewClickedListener(new ToolTipView.OnToolTipViewClickedListener() {
                @Override
                public void onToolTipViewClicked(ToolTipView toolTipView) {
                    toolTipView.remove();
                }
            });


            SharedPreferences.Editor editor = wmbPreference.edit();
            editor.putBoolean("FIRSTRUN", false);
            editor.commit();
        }

    }

    public static void addImage(Bitmap image){
        photos.add(image);
    }

    public static void setImage(Bitmap image){
        if(image==null){
            return;
        }else{
            place_photo.setImageBitmap(image);
            if(isFirstRun) {
                refresh_toolTipRelativeLayout.bringToFront();
                tab_toolTipRelativeLayout.bringToFront();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.refresh) {
            ids.remove(getID());
            Intent intent = getIntent();
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addId(String id){
        ids.add(id);
    }

    public static String getID(){
        return ids.get(random);
    }

    public static void removeID(String id) {
        ids.remove(id);
    }

    public static Restaurant getRestaurant(){
        return restaurant;
    }

    public static ArrayList<Review> getReviews(){
        return reviews;
    }

}