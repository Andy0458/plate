package com.projectplate.plate;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class FindReviews extends Fragment {

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        if(view!=null){
            return view;
        }
        view = inflater.inflate(R.layout.find_reviews, container, false);
        ArrayList<Review> reviews = Find.getReviews();
        Typeface caviardreams = Typeface.createFromAsset(getActivity().getAssets(), "fonts/caviardreams.ttf");


        //Setup Reviews
        TextView user_text1 = (TextView)view.findViewById(R.id.user_text1);
        TextView user_text2 = (TextView)view.findViewById(R.id.user_text2);
        TextView user_text3 = (TextView)view.findViewById(R.id.user_text3);

        //Setup Ratings
        ImageView user_rating1 = (ImageView)view.findViewById(R.id.user_rating1);
        ImageView user_rating2 = (ImageView)view.findViewById(R.id.user_rating2);
        ImageView user_rating3 = (ImageView)view.findViewById(R.id.user_rating3);

        //Setup Names
        TextView user_name1 = (TextView)view.findViewById(R.id.user_name1);
        TextView user_name2 = (TextView)view.findViewById(R.id.user_name2);
        TextView user_name3 = (TextView)view.findViewById(R.id.user_name3);

        //display info
        if(Find.getReviews().size()>=1) {
            user_text1.setText(Html.fromHtml(reviews.get(0).getReview() + "   " +
                    "<a href=\"" + reviews.get(0).getLink() + "\">Read more</a>"
            ));
            user_text1.setLinkTextColor(Color.parseColor("#fd9944"));
            user_text1.setMovementMethod(LinkMovementMethod.getInstance());
            user_text1.setTypeface(caviardreams);
            user_name1.setText(reviews.get(0).getName());
            user_name1.setTypeface(caviardreams);
            setRating(reviews.get(0).getRating(), user_rating1);
        }
        if(Find.getReviews().size()>=2) {
            user_text2.setText(Html.fromHtml(reviews.get(1).getReview() + "   " +
                    "<a href=\"" + reviews.get(1).getLink() + "\">Read more</a>"
            ));
            user_text2.setLinkTextColor(Color.parseColor("#fd9944"));
            user_text2.setMovementMethod(LinkMovementMethod.getInstance());
            user_text2.setTypeface(caviardreams);
            user_name2.setText(reviews.get(1).getName());
            user_name2.setTypeface(caviardreams);
            setRating(reviews.get(1).getRating(), user_rating2);
        }
        if(Find.getReviews().size()==3) {
            user_text3.setText(Html.fromHtml(reviews.get(2).getReview() + "   " +
                    "<a href=\"" + reviews.get(2).getLink() + "\">Read more</a>"
            ));
            user_text3.setLinkTextColor(Color.parseColor("#fd9944"));
            user_text3.setMovementMethod(LinkMovementMethod.getInstance());
            user_text3.setTypeface(caviardreams);
            user_name3.setText(reviews.get(2).getName());
            user_name3.setTypeface(caviardreams);
            setRating(reviews.get(2).getRating(), user_rating3);
        }

        TextView moreReviews = (TextView)view.findViewById(R.id.more_reviews);
        moreReviews.setText(Html.fromHtml("<a href=\""+Find.getRestaurant().getSite()+"\">More Reviews</a>"));
        moreReviews.setLinkTextColor(Color.parseColor("#fd9944"));
        moreReviews.setMovementMethod(LinkMovementMethod.getInstance());
        moreReviews.setTypeface(caviardreams);

        return view;
    }

    public void setRating(int rating, ImageView imageView){

        int[] ratings = {R.drawable.star_0_0, R.drawable.star_1_0, R.drawable.star_2_0, R.drawable.star_3_0,
                R.drawable.star_4_0, R.drawable.star_5_0};
        imageView.setImageResource(ratings[rating]);

    }

}
