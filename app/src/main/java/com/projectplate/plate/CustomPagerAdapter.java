package com.projectplate.plate;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.support.v4.app.FragmentPagerAdapter;

public class CustomPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 3;
    private Context context;
    String[] titles = {"Place", "Reviews", "Map"};

    public CustomPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                FindPlace place = new FindPlace();
                return place;
            case 1:
                FindReviews reviews = new FindReviews();
                return reviews;
            case 2:
                FindMap map = new FindMap();
                return map;
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position){
        return titles[position];
    }
}