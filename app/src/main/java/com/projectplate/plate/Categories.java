package com.projectplate.plate;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by MirShermer on 7/28/2016.
 */
public class Categories extends AppCompatActivity {
    double lat, lon, rad;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.categories);
        final Typeface caviardreams_regular = Typeface.createFromAsset(getAssets(), "fonts/caviardreams.ttf");
        Typeface caviardreams_bold = Typeface.createFromAsset(getAssets(), "fonts/caviardreams_bold.ttf");
        Typeface caviardreams_italic = Typeface.createFromAsset(getAssets(), "fonts/caviardreams_italic.ttf");

        lat = getIntent().getDoubleExtra("latitude", 0);
        lon = getIntent().getDoubleExtra("longitude", 0);
        rad = getIntent().getDoubleExtra("radius", 5*1609.344);

        //Setup drawer layout/toolbar
        DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final LayoutInflater inflater = LayoutInflater.from(this);
        View v = inflater.inflate(R.layout.titleview, null);
        getSupportActionBar().setCustomView(v, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
        ));
        TextView title = (TextView)v.findViewById(R.id.title);
        title.setText("Categories");
        title.setTypeface(caviardreams_regular);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_opened, R.string.drawer_closed);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        //Setup ListView
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
                drawer.closeDrawer(GravityCompat.START);
                if(id==R.id.nav_contact){
                    startActivity(new Intent(Categories.this, ContactUs.class));
                    return true;
                }
                else if(id==R.id.nav_recent){
                    startActivity(new Intent(Categories.this, Recents.class));
                    return true;
                }
                else if(id==R.id.nav_favorites){
                    startActivity(new Intent(Categories.this, Favorites.class));
                    return true;
                }
                else if(id==R.id.nav_share){
                    Snackbar sb = Snackbar.make(drawer, "Functionality Coming Soon!",Snackbar.LENGTH_SHORT);
                    sb.show();
                    TextView message = (TextView) sb.getView().findViewById(android.support.design.R.id.snackbar_text);
                    TextView action = (TextView)sb.getView().findViewById(android.support.design.R.id.snackbar_action);
                    action.setTypeface(caviardreams_regular);
                    message.setTypeface(caviardreams_regular);
                }

                return true;
            }
        });

        //establish buttons
        Button breakfast = (Button) findViewById(R.id.breakfast_button);
        Button lunch = (Button) findViewById(R.id.lunch_button);
        Button dinner = (Button) findViewById(R.id.dinner_button);
        Button drinks = (Button) findViewById(R.id.drinks_button);
        Button surpriseMe = (Button) findViewById(R.id.surprise_button);
        Button dessert = (Button)findViewById(R.id.dessert_button);

        //set typeface on buttons
        breakfast.setTypeface(caviardreams_bold);
        lunch.setTypeface(caviardreams_bold);
        dinner.setTypeface(caviardreams_bold);
        drinks.setTypeface(caviardreams_bold);
        surpriseMe.setTypeface(caviardreams_bold);
        dessert.setTypeface(caviardreams_bold);

        //breakfast clicked
        final Intent intent = new Intent(Categories.this, Find.class);
        intent.putExtra("latitude", lat);
        intent.putExtra("longitude", lon);
        intent.putExtra("radius", rad);
        breakfast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("categories", "breakfast_brunch,waffles");
                startActivity(intent);
            }
        });

        //lunch clicked
        lunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("categories", "foodtrucks,tradamerican,bbq,burgers,delis,hotdogs,mexican,pizza,sandwiches");
                startActivity(intent);
            }
        });

        //dinner clicked
        dinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("categories", "restaurants");
                startActivity(intent);
            }
        });

        //dessert clicked
        dessert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("categories","desserts,icecream");
                startActivity(intent);
            }
        });

        //drink clicked
        drinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("categories", "bars,coffee,cafes,juicebars");
                startActivity(intent);
            }
        });

        //surprise me clicked
        surpriseMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("categories", "");
                startActivity(intent);
            }
        });
    }
}
