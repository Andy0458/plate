package com.projectplate.plate;

/**
 * Created by andre on 8/12/2016.
 */
public class Review {
    String name, review, photo, link;
    int rating;
    public Review(String name, String review, String photo, String link, int rating) {
        this.name = name;
        this.review = review;
        this.photo = photo;
        this.link = link;
        this.rating = rating;
    }

    public String getName(){
        return name;
    }

    public String getReview(){
        return review;
    }

    public String getPhoto(){
        return photo;
    }

    public String getLink(){
        return link;
    }

    public int getRating(){
        return rating;
    }

}
