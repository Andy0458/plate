package com.projectplate.plate;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class Recents extends AppCompatActivity {

    Typeface caviardreams_regular;
    DrawerLayout drawerLayout;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);


        setContentView(R.layout.recent_spins);
        caviardreams_regular = Typeface.createFromAsset(getAssets(), "fonts/caviardreams.ttf");

        //Setup drawer layout/toolbar
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LayoutInflater inflater = LayoutInflater.from(this);
        View v = inflater.inflate(R.layout.titleview, null);
        getSupportActionBar().setCustomView(v, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
        ));
        TextView title = (TextView)v.findViewById(R.id.title);
        title.setText("Recent Spins");
        title.setTypeface(caviardreams_regular);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_opened, R.string.drawer_closed);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        //Setup ListView
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
                drawer.closeDrawer(GravityCompat.START);
                if(id==R.id.nav_contact){
                    startActivity(new Intent(Recents.this, ContactUs.class));
                    finish();
                    return true;
                }
                else if(id==R.id.nav_recent){

                }
                else if(id==R.id.nav_favorites){
                    startActivity(new Intent(Recents.this, Favorites.class));
                    finish();
                    return true;
                }
                else if(id==R.id.nav_share){
                    Snackbar sb = Snackbar.make(drawer, "Functionality Coming Soon!",Snackbar.LENGTH_SHORT);
                    sb.show();
                    TextView message = (TextView) sb.getView().findViewById(android.support.design.R.id.snackbar_text);
                    TextView action = (TextView)sb.getView().findViewById(android.support.design.R.id.snackbar_action);
                    action.setTypeface(caviardreams_regular);
                    message.setTypeface(caviardreams_regular);
                }

                return true;
            }
        });

    }


}
