package com.projectplate.plate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by andrew on 1/3/2017.
 */
public class CustomJSONObject{

    static File recent_spins_file, favorites_file;
    static JSONObject recent_spins = new JSONObject(), favorites = new JSONObject();
    static String json;
    static FileWriter fileWriter;

    static void initialize(){
        recent_spins_file = new File("storage/emulated/0/Android/data/com.projectplate.plate/recent.json");
        favorites_file = new File("storage/emulated/0/Android/data/com.projectplate.plate/favorites.json");
        System.out.println(recent_spins_file.exists());
        if(!recent_spins_file.exists())
            try {
                recent_spins.put("recent", new JSONArray());
                recent_spins_file.createNewFile();
            } catch (Exception e) {
                e.printStackTrace();
            }
        if(!favorites_file.exists()){
            try{
                favorites.put("favorites", new JSONArray());
                favorites_file.createNewFile();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        try {
            favorites.getJSONArray("favorites");
        }catch (Exception e){
            try {
                favorites.put("favorites", new JSONArray());
            } catch (JSONException e1) {
                e1.printStackTrace();
            }
        }
        try {
            StringBuilder text = new StringBuilder();
            BufferedReader br = null;

            try {
                br = new BufferedReader(new FileReader(recent_spins_file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
            } catch (IOException e) {
                // do exception handling
            } finally {
                try { br.close(); } catch (Exception e) { }
            }
            json = text.toString();
            text = new StringBuilder();
            recent_spins = new JSONObject(json);

            try {
                br = new BufferedReader(new FileReader(favorites_file));
                String line;

                while ((line = br.readLine()) != null) {
                    text.append(line);
                    text.append('\n');
                }
            } catch (IOException e) {
                // do exception handling
            } finally {
                try { br.close(); } catch (Exception e) { }
            }
            json = text.toString();
            favorites = new JSONObject(json);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        updateFiles();
    }

    static void updateFiles(){
        try{
            fileWriter = new FileWriter(recent_spins_file);
            fileWriter.write(recent_spins.toString());
            fileWriter.flush();
            fileWriter.close();

            fileWriter = new FileWriter(favorites_file);
            fileWriter.write(favorites_file.toString());
            fileWriter.flush();
            fileWriter.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    static void addRecent(Restaurant restaurant){
        try {
            JSONObject temp_big = new JSONObject();
            JSONObject temp_small = new JSONObject();
            JSONArray recents = recent_spins.getJSONArray("recent");

            temp_small.put("name", restaurant.getName());
            temp_small.put("address", restaurant.getAddress());
            temp_small.put("rating", restaurant.getRating());
            temp_small.put("site", restaurant.getSite());

            recents.put(temp_small);
            temp_big.put("recent", recents);
            recent_spins = temp_big;
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    static void addFavorite(Restaurant restaurant){
        try {
            JSONObject temp_big = new JSONObject();
            JSONObject temp_small = new JSONObject();
            JSONArray favorites_temp = favorites.getJSONArray("favorites");

            temp_small.put("name", restaurant.getName());
            temp_small.put("address", restaurant.getAddress());
            temp_small.put("rating", restaurant.getRating());
            temp_small.put("site", restaurant.getSite());

            favorites_temp.put(temp_small);
            temp_big.put("favorites", favorites_temp);
            favorites = temp_big;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        printFavorites();
    }

    static void printRecents(){
        try {
            System.out.println("recent: "+ recent_spins.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    static void printFavorites(){
        try {
            System.out.println("favorites: "+ favorites.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
