package com.projectplate.plate;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageSwitcher;
import android.widget.ImageView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;

import java.io.InputStream;

public class DownloadImageTask extends AsyncTask<String, Void, Void> {
    Bitmap[] bitmap;
    String urldisplay;
    public DownloadImageTask() {
    }

    protected Void doInBackground(String... urls) {
        int counter = 0;
        bitmap = new Bitmap[3];
        while(counter<3) {
            if((urldisplay = urls[counter])==null) {
                return null;
            }
            urldisplay = urls[counter];
            if (!urldisplay.contains(".png") && !urldisplay.contains(".jpg") && !urldisplay.contains(".jpeg"))
                urldisplay += ".png";
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                bitmap[counter] = BitmapFactory.decodeStream(in);
                Find.addImage(bitmap[counter]);
                counter++;
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
        return null;
    }

    protected void onPostExecute(Void result) {
        Find.setImage(bitmap[0]);
    }
}