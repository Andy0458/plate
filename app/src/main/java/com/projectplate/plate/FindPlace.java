package com.projectplate.plate;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;

import java.util.Locale;

/**
 * Created by andre on 8/11/2016.
 */
public class FindPlace extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.find_place, container, false);

        Typeface caviardreams_bold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/caviardreams_bold.ttf");
        Typeface caviardreams = Typeface.createFromAsset(getActivity().getAssets(), "fonts/caviardreams.ttf");

        Button open_in_maps = (Button) view.findViewById(R.id.open_in_maps);
        Button invite_friends = (Button) view.findViewById(R.id.invite_friends);
        Button favorite = (Button)view.findViewById(R.id.button_favorite);
        if(Find.getRestaurant().isFavorite()){
            favorite.setBackgroundResource(R.drawable.button_favorite_pressed);
        }
        open_in_maps.setTypeface(caviardreams);
        invite_friends.setTypeface(caviardreams);

        final Restaurant restaurant = Find.getRestaurant();

        open_in_maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?saddr=%f,%f&daddr=%s", Find.getLocation().latitude, Find.getLocation().longitude,
                  //      Find.getRestaurant().getAddress());
                String uri = "geo:"+Find.getRestaurant().getLocation().latitude+","+Find.getRestaurant().getLocation().longitude
                        +"?q="+Uri.encode(Find.getRestaurant().getName());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });

        invite_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Coming Soon", Snackbar.LENGTH_LONG)
                        .show();
            }
        });
        favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Find.getRestaurant().isFavorite()){
                    v.setBackgroundResource(R.drawable.button_favorite_unpressed);
                    Find.getRestaurant().setFavorite(false);
                    try {
                        for(int i = 0; i < CustomJSONObject.favorites.getJSONArray("favorites").length(); i++) {
                            if(Find.getRestaurant().getName().equals(CustomJSONObject.favorites.getJSONArray("favorites").getJSONObject(i).get("name"))
                                    && Find.getRestaurant().getAddress().equals(CustomJSONObject.favorites.getJSONArray("favorites").getJSONObject(i).get("address"))) {
                                System.out.println("ridding of favorite...");
                                CustomJSONObject.favorites.getJSONArray("favorites").remove(i);
                                break;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    v.setBackgroundResource(R.drawable.button_favorite_pressed);
                    Find.getRestaurant().setFavorite(true);
                    CustomJSONObject.addFavorite(restaurant);
                }
            }
        });

        FontFitTextView name = (FontFitTextView) view.findViewById(R.id.name);
        name.setText(restaurant.getName());
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uriUrl = Uri.parse(restaurant.getSite());
                Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
                startActivity(launchBrowser);
            }
        });
        //name.setMovementMethod(LinkMovementMethod.getInstance());
        name.setTypeface(caviardreams_bold);
        TextView address = (TextView) view.findViewById(R.id.address);
        address.setText(restaurant.getAddress());
        address.setTypeface(caviardreams);
        final TextView phone = (TextView) view.findViewById(R.id.phone);
        phone.setTypeface(caviardreams);
        phone.setText(restaurant.getPhone());
        phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phone.getText()));
                startActivity(intent);
            }
        });
        TextView hours = (TextView)view.findViewById(R.id.hours);
        hours.setText(restaurant.getHours());
        hours.setTypeface(caviardreams);
        ImageView rating =(ImageView) view.findViewById(R.id.rating);

        int[] ratings = {R.drawable.star_0_0, R.drawable.star_0_0, R.drawable.star_1_0, R.drawable.star_1_5,
                            R.drawable.star_2_0, R.drawable.star_2_5, R.drawable.star_3_0, R.drawable.star_3_5,
                            R.drawable.star_4_0, R.drawable.star_4_5, R.drawable.star_5_0};
        rating.setImageResource(ratings[(int)(restaurant.getRating()/0.5)]);

        TextView numReviews = (TextView)view.findViewById(R.id.numReviews);
        numReviews.setTypeface(caviardreams);
        numReviews.setText(restaurant.getNumReviews()+" reviews");

        return view;
    }
}
