package com.projectplate.plate;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import android.location.Geocoder;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by andre on 6/19/2016.
 */
public class Map extends AppCompatActivity implements LocationListener {
    double rad = 0;
    private GoogleMap mMap;
    Boolean locationWanted;
    static LatLng latLng;
    String place;
    Criteria criteria;
    String bestProvider;
    LocationManager locationManager;
    public static float radiusP;
    Intent find;
    Typeface caviardreams_bold, caviardreams;
    ProgressBar progress;
    TextView progress_text;
    Location location;
    RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent pastIntent = getIntent();

        locationWanted = pastIntent.getBooleanExtra("location", false);
        place = pastIntent.getStringExtra("place");
        setContentView(R.layout.map);
        caviardreams = Typeface.createFromAsset(getAssets(), "fonts/caviardreams.ttf");

        progress = (ProgressBar)findViewById(R.id.progress);
        progress_text = (TextView)findViewById(R.id.progress_text);
        progress_text.setTypeface(caviardreams);

        //Setup drawer layout/toolbar
        DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        LayoutInflater inflater = LayoutInflater.from(this);
        View v = inflater.inflate(R.layout.titleview, null);
        getSupportActionBar().setCustomView(v, new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.MATCH_PARENT,
                Gravity.CENTER
        ));
        TextView title = (TextView)v.findViewById(R.id.title);
        title.setText("Distance");
        title.setTypeface(caviardreams);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_opened, R.string.drawer_closed);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        //Setup ListView
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                int id = item.getItemId();
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer);
                drawer.closeDrawer(GravityCompat.START);
                if(id==R.id.nav_contact){
                    startActivity(new Intent(Map.this, ContactUs.class));
                    return true;
                }
                else if(id==R.id.nav_recent){
                    startActivity(new Intent(Map.this, Recents.class));
                    return true;
                }
                else if(id==R.id.nav_favorites){
                    startActivity(new Intent(Map.this, Favorites.class));
                    return true;
                }
                else if(id==R.id.nav_share){
                    Snackbar sb = Snackbar.make(drawer, "Functionality Coming Soon!",Snackbar.LENGTH_SHORT);
                    sb.show();
                    TextView message = (TextView) sb.getView().findViewById(android.support.design.R.id.snackbar_text);
                    TextView action = (TextView)sb.getView().findViewById(android.support.design.R.id.snackbar_action);
                    action.setTypeface(caviardreams);
                    message.setTypeface(caviardreams);
                }

                return true;
            }
        });


        try {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            if(locationWanted) {
                locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                locationManager.requestLocationUpdates(locationManager.getBestProvider(new Criteria(), true), 1000, 0, this);
                layout = (RelativeLayout)findViewById(R.id.loading);
                drawerLayout.bringToFront();
                layout.bringToFront();
            }
            else {
                Location location = new Location("");
                if (Geocoder.isPresent()) {
                    try {
                        Geocoder gc = new Geocoder(this);
                        List<Address> addresses = gc.getFromLocationName(place, 5); // get the found Address Objects
                        for (Address a : addresses) {
                            if (a.hasLatitude() && a.hasLongitude()) {
                                latLng = new LatLng(a.getLatitude(), a.getLongitude());
                            }
                        }
                        location.setLatitude(latLng.latitude);
                        location.setLongitude(latLng.longitude);
                        layout = (RelativeLayout)findViewById(R.id.loading);
                        drawerLayout.bringToFront();
                        layout.bringToFront();
                        onLocationChanged(location);
                    } catch (IOException e) {
                        // handle the exception
                    }
                }
            }
        }catch (SecurityException e){
            e.printStackTrace();
        }

    }

    public void setupUI(){
        layout.setVisibility(View.GONE);

        SeekBar seekBar = (SeekBar) findViewById(R.id.seekbar);
        final TextView radius = (TextView) findViewById(R.id.radius);
        radius.setVisibility(View.VISIBLE);
        radius.setTypeface(caviardreams);
        seekBar.setMax(50);
        seekBar.setVisibility(View.VISIBLE);
        Button button = (Button)findViewById(R.id.confirm);
        button.setVisibility(View.VISIBLE);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                rad = progress / 2.0;
                radiusP = progress / 2.0f;
                radius.setText("Radius: " + progress / 2.0 + " Miles");
                mMap.clear();
                CircleOptions circleOptions = new CircleOptions();
                circleOptions.center(latLng);
                circleOptions.radius(rad * 1609.34);
                circleOptions.strokeColor(0xb3c6ee);
                // 0x = Hex code, 55 = transparency value, 001EFF = color in hex
                circleOptions.fillColor(0x55001EFF);
                mMap.addCircle(circleOptions);
                mMap.addMarker(new MarkerOptions().position(getLocation())
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                radius.setVisibility(View.VISIBLE);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        find = new Intent(this, Categories.class);

        find.putExtra("location", locationWanted);
        if (latLng != null) {
            find.putExtra("latitude", latLng.latitude);
            find.putExtra("longitude", latLng.longitude);
        }

        caviardreams_bold = Typeface.createFromAsset(getAssets(), "fonts/caviardreams_bold.ttf");


    }

    @Override
    public void onLocationChanged(Location location) {
        if(location==null){
            return;
        }

        mMap.clear();
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        latLng = new LatLng(latitude, longitude);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
        MarkerOptions markerOptions = new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
        mMap.addMarker(markerOptions);
        setupUI();
        try {
            if(locationWanted) {
                locationManager.removeUpdates(this);
            }
        }catch (SecurityException e){
            e.printStackTrace();
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void onConfirmClick(View view) {
        System.out.println(rad);
        find.putExtra("radius", getMeters(rad));
        startActivity(find);
    }

    public static double getMeters(double f) {
        return f * 1069.344;
    }

    public static LatLng getLocation() {
        return latLng;
    }

}
