package com.projectplate.plate;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * Created by andrew on 7/11/16.
 */
public class Restaurant{
    private String name, address, phone, site, hours;
    private double rating;
    boolean favorite;
    LatLng location;
    private ArrayList<String> photoURL;
    private int numReviews;
    public Restaurant(String name, String address, String phone, String site, String hours, ArrayList<String> photos, double rating, int numReviews, LatLng location){
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.site = site;
        this.hours = hours;
        this.photoURL = photos;
        this.rating = rating;
        this.numReviews = numReviews;
        this.location = location;
        favorite = false;
    }

    public int getNumReviews(){
        return numReviews;
    }

    public void setFavorite(boolean b){
        favorite = b;
    }

    public String getHours(){
        return hours;
    }

    public double getRating(){
        return rating;
    }

    public boolean isFavorite(){
        return favorite;
    }

    public LatLng getLocation(){
        return location;
    }

    public String getName(){
        return name;
    }

    public String getAddress(){
        return address;
    }

    public ArrayList<String> getPhotoURL(){
        return photoURL;
    }

    public String getPhone(){
        return phone;
    }

    public String getSite(){
        return site;
    }

    public String toString(){
        return name + ", " + address;
    }
}
