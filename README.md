![icon.png](https://bitbucket.org/repo/ALobBj/images/3494571626-icon.png)

# Overview #

Plate combines social interaction and food. Plate is an Android application that combines the simplicity of finding a random restaurant in the area with a sleek design and playful mechanics.

### Specifications ###

* Will run on all Android models operating on the Android 5.0 (Lollipop) OS or higher
* User Invitation
* Social Media Networking